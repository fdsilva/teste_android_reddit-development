package com.fastnews.service

enum class NetworkState {
    RUNNING,
    SUCCESS,
    FAILED
}